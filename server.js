const { RSocketServer } = require("rsocket-core");
const RSocketWebSocketServer = require("rsocket-websocket-server");
const { Single } = require("rsocket-flowable");
const mysql = require("mysql");

const WebSocketTransport = RSocketWebSocketServer.default;
const host = "127.0.0.1";
const port = 7000;

const transportOpts = {
  host: host,
  port: port,
};

const transport = new WebSocketTransport(transportOpts);

const statuses = {
  PENDING: "pending",
  CANCELLED: "cancelled",
};

var connection = mysql.createConnection({
  host: "localhost",
  user: "rsocket",
  password: "rsocket",
  database: "rsocket",
  insecureAuth: false,
});

connection.connect();

connection.query(
  "SELECT count(*) as count from users",
  function (error, results, fields) {
    if (error) throw error;
    console.log("Users count: ", results[0].count);
  }
);

const getRequestHandler = (requestingRSocket, setupPayload) => {
  function handleRequestResponse(payload) {
    let status = statuses.PENDING;
    const { data, metadata } = payload;

    return new Single((subscriber) => {
      function handleCancellation() {
        status = statuses.CANCELLED;
      }

      subscriber.onSubscribe(() => handleCancellation());

      if (metadata.includes("createUser")) {
        const { username, password } = JSON.parse(data);
        var sql = `INSERT INTO users (username, password) VALUES ('${username}', '${password}')`;
        connection.query(sql, function (err, result) {
          if (err) throw err;

          const response = {
            status: "SUCCESS",
            data: {
              id: result.insertId,
            },
          };

          subscriber.onComplete({
            data: JSON.stringify(response),
          });
        });

        return;
      } else {
        const { id } = JSON.parse(data);
        var sql = `UPDATE users SET count = count + 1 where id=${id}`;
        connection.query(sql, function (err, result) {
          if (err) throw err;
          const response = {
            status: "SUCCESS",
            data: {
              counter: 1,
            },
          };
          subscriber.onComplete({
            data: JSON.stringify(response),
          });
        });
      }
    });
  }

  return {
    requestResponse: handleRequestResponse,
  };
};

const rSocketServer = new RSocketServer({
  transport,
  getRequestHandler,
});

console.log(`Server starting on port ${port}...`);
rSocketServer.start();
